# khala-game Project

Khala Game Rest BE for Bol's challenge

0. GameService-initialize()->BoardState-applyMoves()->Moves-convertMove2cellpieces()->Row-includes->Cells||treasureCell

1. 1.Game Service Initializes a game include of players, Board. //Extensible Service JoinPlayer etc. 2.Board State
   Service // status service of Khala Game Board, includes game move and rules  "Core Logic"
   MoveRequest is the general format of Khala Game, including Cells, treasureCell(treasure)
2. 2 number of players
3. Each player has his own Row of Cells including 6cells for " shared in game"  and +1 treasureCell to collect pieces.
   ...
4. The game will be initialized Each cell has number of (6) pieces in it, except Zero Cells.
5. Zero Cell is the name of a Cell that initialized as 0 pieces. throws exception illegal cellIndexMove
6. Player can choose to move from his row except Zero Cell. MoveIndexCell should include a piece. Throws Exception
7. Turn Passing between players, The cells should be filled by pieces of other player row. OpenApi 3.0 Specifications

MongoDB Embedded- -Board Games have less functional requirements in persisstance layer to use relational database.
-Mostly storing and searching an object is the main goal, MongoDB is generaly known for this capabilities in high
performance.

-To-Do's

0. Game-User-Person Implementation, to Join game and Lobby features
1. Kong API GW - Authenticate, secure OWASP Vulnerabilities,
2. RBAC - KeyCloak as a Authorization Manager To Authorize and Authenticate over Person DB
3. Jaeger traceability
4. Eventsourcing with Kafka and CQRS to improve performance in microservice architecture. (Availability)
5. ELK for LogStore
7. Cloud enablement with native DB ( DynamoDB) orchestrator
8. UI according to OpenApi v3 specified definition document
9. Game-User correlation, roles based auhorization

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:

```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:

```shell script
./mvnw package
```

It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory. Be aware that it’s not an _über-jar_ as
the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:

```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using:

```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using:

```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/khala-game-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- REST resources for MongoDB with Panache ([guide](https://quarkus.io/guides/rest-data-panache)): Generate JAX-RS
  resources for your MongoDB entities and repositories
- MongoDB with Panache ([guide](https://quarkus.io/guides/mongodb-panache)): Simplify your persistence code for MongoDB
  via the active record or the repository pattern
- YAML Configuration ([guide](https://quarkus.io/guides/config#yaml)): Use YAML to configure your Quarkus application
- RESTEasy JAX-RS ([guide](https://quarkus.io/guides/rest-json)): REST endpoint framework implementing JAX-RS and more
- SmallRye JWT ([guide](https://quarkus.io/guides/security-jwt)): Secure your applications with JSON Web Token
- Logging JSON ([guide](https://quarkus.io/guides/logging#json-logging)): Add JSON formatter for console logging
- SmallRye Health ([guide](https://quarkus.io/guides/microprofile-health)): Monitor service health
- SmallRye OpenTracing ([guide](https://quarkus.io/guides/opentracing)): Trace your services with SmallRye OpenTracing
- MongoDB client ([guide](https://quarkus.io/guides/mongodb)): Connect to MongoDB in either imperative or reactive style
- Keycloak Authorization ([guide](https://quarkus.io/guides/security-keycloak-authorization)): Policy enforcer using
  Keycloak-managed permissions to control access to protected resources
- SmallRye OpenAPI ([guide](https://quarkus.io/guides/openapi-swaggerui)): Document your REST APIs with OpenAPI - comes
  with Swagger UI
- Cache ([guide](https://quarkus.io/guides/cache)): Enable application data caching in CDI beans

## Provided Code

### YAML Config

Configure your application with YAML

[Related guide section...](https://quarkus.io/guides/config-reference#configuration-examples)

The Quarkus application configuration is located in `src/main/resources/application.yml`.

### RESTEasy JAX-RS

Easily start your RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started#the-jax-rs-resources)

### SmallRye Health

Monitor your application's health using SmallRye Health

[Related guide section...](https://quarkus.io/guides/smallrye-health)
