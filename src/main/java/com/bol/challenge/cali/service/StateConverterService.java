package com.bol.challenge.cali.service;

import com.bol.challenge.cali.entity.BoardState;
import com.bol.challenge.cali.entity.Cell;
import com.bol.challenge.cali.enums.Turn;
import com.bol.challenge.cali.exception.EmptyCellException;
import com.bol.challenge.cali.exception.IllegalMoveException;
import com.bol.challenge.cali.exception.UnauthorizedPlayerException;
import com.bol.challenge.cali.repository.BoardStateRepository;
import com.bol.challenge.cali.repository.CellService;
import com.bol.challenge.cali.repository.MoveConverter;
import com.bol.challenge.cali.request.MoveRequest;
import com.bol.challenge.cali.request.NextMove;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.NotFoundException;


@ApplicationScoped
@RequiredArgsConstructor
public class StateConverterService {
   
    @NonNull
    private BoardStateRepository stateRepo;


    public BoardState createWithGameId(String gameId){
            BoardState state = new BoardState();
            state. setGameId(gameId);
            stateRepo.persist(state);
        return stateRepo.findByGameId(gameId);
    }
    public BoardState create() {
        BoardState boardstate = new BoardState();
                  stateRepo.persist(boardstate);
        return  stateRepo.findById(boardstate.id);
    }

    public BoardState findById(final String id) {
        return stateRepo.findById(new ObjectId(id));
    }
    public void move(final MoveRequest request) {

        BoardState board = stateRepo.find(request.getId()).firstResultOptional().orElseThrow(NotFoundException::new);

        isValidMove(request, board);

        MoveConverter converter = MoveConverter.builder()
                .nextMove(NextMove.builder()
                        .index(request.getCellIndex() + 1)
                        .turn(request.getCurrentPlayer())
                        .build())
                .pieces(CellService.grabAllPiecesInCell(request, board))
                .build();

        stateRepo.persist(settlePieces(board, converter));

    }

    public BoardState settlePieces(BoardState state, MoveConverter converter) {
        if (converter.getUpdatedPieces() > 0) {
            updateCells(state, converter);
        }
        return state;
    }

    public void updateCells(BoardState state, MoveConverter moveConverter) {

        Cell cell = CellService.getCell(moveConverter.getNextMove(), state);

        if (moveConverter.getUpdatedPieces() == 1 && cell.getPieces().equals(0) && !cell.isTreasureCell()) {
            cell.setPieces(CellService.captureOpponentPiece(state, moveConverter));
        } else {
            cell.setPieces(cell.getPieces() + 1);
        }
    }

    public void isValidMove(MoveRequest request, BoardState state) {

        isValidActiveGame(state);
        isValidTurn(state, request.getCurrentPlayer());
        isValidTreasureCell(request);
        isValidCellPieces(state, request);
    }

    private void isValidCellPieces(BoardState state, MoveRequest request) {
    if (CellService.getCell(request, state).getPieces() < 1) {
      throw new EmptyCellException();
        }
    }

    private void isValidTreasureCell(MoveRequest request) {
        if(request.getCellIndex().equals(CellService.TREASURE_INDEX)){
            throw new IllegalMoveException();
        }
    }

    private void isValidTurn(BoardState state, Turn currentPlayer) {
    if (!state.getNextTurn().equals(currentPlayer)) {
      throw new UnauthorizedPlayerException();
        }
    }

    private void isValidActiveGame(BoardState state) {
        if(state.isWinnerFound()){
            throw new NotFoundException();
        }
    }
}