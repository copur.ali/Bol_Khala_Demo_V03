package com.bol.challenge.cali.request;

import com.bol.challenge.cali.enums.Turn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.With;

@Getter
@Builder
@With
@Schema(description = "Move")
public class NextMove {
Turn turn;
Integer index;
}
