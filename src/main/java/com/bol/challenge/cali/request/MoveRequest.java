package com.bol.challenge.cali.request;

import com.bol.challenge.cali.enums.Turn;
import io.smallrye.common.constraint.NotNull;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.With;

import javax.validation.constraints.Max;
import javax.validation.constraints.PositiveOrZero;
@Getter
@Builder
@Schema(description = "Move")
public class MoveRequest {


    @With
    @Schema(hidden = true)
    private String id;

    @NotNull
    @Schema(example = "ONE")
    private Turn currentPlayer;

    @Max(5)
    @NotNull
    @PositiveOrZero
    @Schema(example = "5")
    private Integer cellIndex;

}
