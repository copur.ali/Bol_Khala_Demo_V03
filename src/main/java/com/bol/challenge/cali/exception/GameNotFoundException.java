package com.bol.challenge.cali.exception;

import javax.ws.rs.NotFoundException;

public class GameNotFoundException extends NotFoundException {

  public   GameNotFoundException(String gameId) {

    super(("Game is not available with Id:" + gameId));
  }
}
