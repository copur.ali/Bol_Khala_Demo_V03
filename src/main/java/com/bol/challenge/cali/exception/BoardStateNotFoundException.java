package com.bol.challenge.cali.exception;


import javax.ws.rs.NotFoundException;

public class BoardStateNotFoundException extends NotFoundException {

  public BoardStateNotFoundException(String reasonId) {

    super( "Board State  is not available" + reasonId);
  }
}
