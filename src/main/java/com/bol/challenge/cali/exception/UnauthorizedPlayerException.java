package com.bol.challenge.cali.exception;


import org.apache.http.HttpStatus;

import javax.ws.rs.NotAuthorizedException;

public class UnauthorizedPlayerException extends NotAuthorizedException {

  public UnauthorizedPlayerException() {

    super(HttpStatus.SC_UNAUTHORIZED, "Move is not Acceptable, Wait your turn!");
  }
}
