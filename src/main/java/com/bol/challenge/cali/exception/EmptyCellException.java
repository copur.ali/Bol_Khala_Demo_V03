package com.bol.challenge.cali.exception;

import javax.ws.rs.BadRequestException;

public class EmptyCellException extends BadRequestException {
  public EmptyCellException() {

    super("EmptyCell is found");
  }
}
