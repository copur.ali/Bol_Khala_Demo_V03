package com.bol.challenge.cali.exception;

import javax.ws.rs.NotFoundException;

public class IllegalMoveException extends NotFoundException {
  public IllegalMoveException() {
    super(("Move is not Acceptable" ));
  }
}
