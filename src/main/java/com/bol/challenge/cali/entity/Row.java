package com.bol.challenge.cali.entity;

import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
public class Row {

        private List<Cell> cells;
        private TreasureCell treasureCell;

        public Row() {
        this.cells = IntStream.range(0, 6).mapToObj(i -> new Cell()).collect(Collectors.toList());
        this.treasureCell = new TreasureCell();
       }

        public boolean isZeroSum() {
        return cells.stream().allMatch(cell -> cell.getPieces().equals(0));
      }

        public void finish() {
        this.getTreasureCell()
                .setPieces(
                        this.getTreasureCell().getPieces()
                                + cells.stream().mapToInt(Cell::getPieces).sum());
        cells.forEach(cell -> cell.setPieces(0));
    }

}
