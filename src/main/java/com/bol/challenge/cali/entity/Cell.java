package com.bol.challenge.cali.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;

@With
@Data
@AllArgsConstructor
public class Cell {

    protected Integer pieces;

    public Cell(){
        this.pieces = 6;
    }

    public boolean isTreasureCell(){
        return false;
    }
}
