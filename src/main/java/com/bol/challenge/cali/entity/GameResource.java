package com.bol.challenge.cali.entity;

import com.bol.challenge.cali.service.StateConverterService;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/bol/games")
@Consumes("application/json")
@Produces("application/json")
public class GameResource {
    @Inject StateConverterService stateConverterService;
    @GET
    public List<Game> list() {
        return Game.listAll();
    }

    @GET
    @Path("/{id}")
    public Game get(@PathParam("id") String id) {
        return Game.findById(new ObjectId(id));
    }

    @POST
    public Response create(Game game) {
        game.persist();

        return Response.ok().entity(stateConverterService.createWithGameId(game.gameId)).build();
    }

    @PUT
    @Path("/{id}")
    public void update(@PathParam("id") String id, Game game) {
        game.update();
    }

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") String id) {
        Game game = Game.findById(new ObjectId(id));
        game.delete();
    }

    @GET
    @Path("/search/{gameId}")
    public Game search(@PathParam("gameId") String gameId) {
        return Game.findByGameId(gameId);
    }

    @DELETE
    public void deleteAll(){
        Person.deleteAll();
    }
}
