package com.bol.challenge.cali.entity;

import com.bol.challenge.cali.enums.Turn;
import io.quarkus.mongodb.panache.common.MongoEntity;
import lombok.Data;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

@Data
@MongoEntity(collection = "gameBoardStates", database = "khala_game")
public class BoardState {


    public ObjectId id;

    private Row firstPlayerRow;
    private Row secondPlayerRow;
    private Turn nextTurn;
    private boolean isWinnerFound;


    @BsonProperty("gameId")
    String gameId;

    // return name as lowercase in the model
    public String getGameId() {
        return gameId.toLowerCase();
    }

    // store all names in uppercase in the DB
    public void setGameId(String gameId) {
        this.gameId = gameId.toUpperCase();
    }

    public boolean isZeroRow() {
    if (firstPlayerRow != null && secondPlayerRow != null)
      return firstPlayerRow.isZeroSum() || secondPlayerRow.isZeroSum();
    return false;
    }

    public void setNextTurn() {
        if(this.nextTurn.equals(Turn.FIRST_PLAYER)){
            this.nextTurn = Turn.SECOND_PLAYER;
        } else {
            this.nextTurn = Turn.FIRST_PLAYER;
        }
    }

    public void finishGame() {
        this.firstPlayerRow.finish();
        this.secondPlayerRow.finish();
        this.isWinnerFound = true;
    }
}