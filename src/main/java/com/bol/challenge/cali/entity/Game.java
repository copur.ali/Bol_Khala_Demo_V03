package com.bol.challenge.cali.entity;

import com.bol.challenge.cali.enums.GameActivity;
import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.time.LocalDateTime;
import java.util.List;

@MongoEntity(collection = "TheKhalaGame", database = "games")
public class Game extends PanacheMongoEntity {
    public String gameId;

    // will be persisted as a 'createdDate' field in MongoDB
    @BsonProperty("stateId")
    public String stateId;

    public GameActivity activity;


    @BsonProperty("createDate")
    public LocalDateTime createDate;
    public Person player1;
    public Person player2;
    // return name as lowercase in the model
    public String getGameId() {
        return gameId.toLowerCase();
    }

    // store all names in uppercase in the DB
    public void setGameId(String gameId) {
        this.gameId = gameId.toUpperCase();
    }

    // entity methods
    public static Game findByGameId(String gameId) {
        return find("gameId", gameId).firstResult();
    }

    public static List<Game> findActive() {
        return list("activity", GameActivity.ACTIVE);
    }

    public static void deleteSuspendedGames() {
        delete("activity", "SUSPENDED");
    }
}
