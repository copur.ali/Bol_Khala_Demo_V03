package com.bol.challenge.cali.entity;

import io.quarkus.arc.Priority;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.enterprise.inject.Alternative;

@Data
@EqualsAndHashCode(callSuper = true)
public class TreasureCell extends Cell {
    public TreasureCell(){
        super();
        this.pieces=0;
    }
    @Alternative
    @Priority(1)
    public  boolean isTreaasureCell(){
        return true;
    }
}
