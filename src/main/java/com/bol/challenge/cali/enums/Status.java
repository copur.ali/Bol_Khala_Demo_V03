package com.bol.challenge.cali.enums;

public enum Status {
    LIVING,
    DECEASED
}
