package com.bol.challenge.cali.enums;

public enum Turn {
  FIRST_PLAYER,
  SECOND_PLAYER
}
