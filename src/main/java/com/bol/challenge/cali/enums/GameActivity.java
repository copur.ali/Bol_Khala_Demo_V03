package com.bol.challenge.cali.enums;

public enum GameActivity {
  ACTIVE,
  CREATED,
  SOLO_CREATED,
  SUSPENDED,
  COMPLETED

}
