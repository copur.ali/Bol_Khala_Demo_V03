package com.bol.challenge.cali.repository;

import com.bol.challenge.cali.entity.BoardState;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class BoardStateRepository implements PanacheMongoRepository<BoardState> {

    public BoardState findByGameId(String gameId) {
        return find("gameId", gameId).firstResult();
    }

    public List<BoardState> findUnfinishedList() {
        return list("isWinnerFound", false);
    }

    public void deleteByGameId(String gameId) {
        delete("gameId", gameId);
    }

    public List<BoardState> findOrdered() {
        return findAll(Sort.by("lastname", "firstname")).list();
    }


}
