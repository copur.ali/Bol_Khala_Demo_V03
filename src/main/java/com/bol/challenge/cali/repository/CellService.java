package com.bol.challenge.cali.repository;

import com.bol.challenge.cali.entity.BoardState;
import com.bol.challenge.cali.entity.Cell;
import com.bol.challenge.cali.entity.Row;
import com.bol.challenge.cali.enums.Turn;
import com.bol.challenge.cali.request.MoveRequest;
import com.bol.challenge.cali.request.NextMove;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class CellService {


    public static final Integer TREASURE_INDEX = 6;
    public static final Integer INITIAL_CELL_INDEX = 0;

    public static Integer grabAllPiecesInCell(MoveRequest request, BoardState state) {
        Cell cell = getCell(request, state);

        Integer stones = cell.getPieces();
        cell.setPieces(0);
        return stones;
    }

    public static Cell getCell(NextMove nextMove, BoardState state){
    if (Turn.FIRST_PLAYER.equals(nextMove.getTurn())) {
            return TREASURE_INDEX.equals(nextMove.getIndex()) ?
                    state.getFirstPlayerRow().getTreasureCell() :
                    getCell(state.getFirstPlayerRow(), nextMove.getIndex());
        } else {
            return TREASURE_INDEX.equals(nextMove.getIndex()) ?
                    state.getSecondPlayerRow().getTreasureCell() :
                    getCell(state.getSecondPlayerRow(), nextMove.getIndex());
        }
    }

    public static Cell getCell(MoveRequest move, BoardState state) {
        return Turn.FIRST_PLAYER.equals(move.getCurrentPlayer()) ?
                getCell(state.getFirstPlayerRow(), move.getCellIndex()) :
                getCell(state.getSecondPlayerRow(), move.getCellIndex());
    }

    private static Cell getCell(Row firstPlayer, Integer index) {
        return firstPlayer.getCells().get(index);
    }

    public static Integer captureOpponentPiece(BoardState state, MoveConverter moveConverter) {
        Cell opponent = getOpponentRow(moveConverter, state);
        Integer pieces = opponent.getPieces() + 1;
        opponent.setPieces(0);

        return pieces;
    }

    private static Cell getOpponentRow(MoveConverter moveConverter, BoardState state) {
        if(Turn.FIRST_PLAYER.equals(moveConverter.getNextMove().getTurn())){
            return getCell(
                    NextMove.builder()
                            .index(CellService.getOpponentCellIndex(moveConverter.getNextMove().getIndex()))
                            .turn(Turn.SECOND_PLAYER)
                            .build(),
                    state);
        } else {
            return getCell(
                    NextMove.builder()
                            .index(CellService.getOpponentCellIndex(moveConverter.getNextMove().getIndex()))
                            .turn(Turn.FIRST_PLAYER)
                            .build(),
                    state);
        }
    }

    private static Integer getOpponentCellIndex(Integer index) {
        return 5 - index;
    }

}
