package com.bol.challenge.cali.repository;

import com.bol.challenge.cali.enums.Turn;
import com.bol.challenge.cali.request.NextMove;
import lombok.Builder;
import lombok.With;

@Builder
@With
public class MoveConverter {
    private NextMove nextMove;
    private Integer pieces;

    public MoveConverter move() {
        return this.withNextMove(getNextMove()).withPieces(getUpdatedPieces());
    }

    public NextMove getNextMove() {
        if(nextMove.getIndex().equals(CellService.TREASURE_INDEX)){
            return NextMove.builder()
                    .turn(nextMove.getTurn().equals(Turn.FIRST_PLAYER) ? Turn.SECOND_PLAYER : Turn.FIRST_PLAYER)
                    .index(0)
                    .build();
        } else {
            return nextMove.withIndex(nextMove.getIndex() + 1);
        }
    }

    public Integer getUpdatedPieces() {
        return this.pieces - 1;
    }

}
