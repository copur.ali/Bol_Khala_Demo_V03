package com.bol.challenge.cali;

import com.bol.challenge.cali.entity.BoardState;
import com.bol.challenge.cali.repository.BoardStateRepository;
import com.bol.challenge.cali.request.MoveRequest;
import com.bol.challenge.cali.service.StateConverterService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/game/boardstate")
@Consumes("application/json")
@Produces("application/json")
public class BoardStateResource {
    @Inject
    StateConverterService stateService;

    @Inject
    BoardStateRepository stateRepository;

    @ApiResponses(value = {

            @ApiResponse(responseCode =  "200", description = "State Board List retrieved successfully"),
            @ApiResponse(responseCode =  "404", description = " no State Board found")
    })
    @GET
    public List<BoardState> list() {
        return stateRepository.listAll();
    }

    @ApiResponses(value = {

            @ApiResponse(responseCode =  "200", description = "Board retrieved successfully"),
            @ApiResponse(responseCode =  "404", description = "Board not found")
    })
    @GET
    @Path("/{id}")
    public BoardState get(@PathParam("id") String id) {
        System.out.println("get =>" + id);
        return stateRepository.findById(new ObjectId(id));
    }
    @ApiResponses(value = {

            @ApiResponse(responseCode =  "200", description = "Board created successfully"),
            @ApiResponse(responseCode =  "404", description = "unable to create Board")
    })
    @POST
    public Response create(BoardState boardState) {
        stateRepository.persist(boardState);
        return Response.status(201).build();
    }
    @ApiResponses(value = {

            @ApiResponse(responseCode =  "200", description = "State Board updated successfully"),
            @ApiResponse(responseCode =  "404", description = "State Board not found")
    })
    @PUT
    @Path("/{id}")
    public void move(@PathParam("id") String id, MoveRequest moveRequest) {
        stateService.move(moveRequest);
    }

    @ApiResponses(value = {

            @ApiResponse(responseCode =  "200", description = "State Board deleted successfully"),
            @ApiResponse(responseCode =  "404", description = "State Board not found")
    })
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") String id) {
        BoardState state = stateRepository.findById(new ObjectId(id));
        stateRepository.delete(state);
    }

    @ApiResponses(value = {

            @ApiResponse(responseCode =  "200", description = "State Board retrieved successfully"),
            @ApiResponse(responseCode =  "404", description = "State Board not found")
    })
    @GET
    @Path("/search/{gameId}")
    public BoardState search(@PathParam("gameId") String gameId) {
        return stateRepository.findByGameId(gameId);
    }
    @ApiResponses(value = {

            @ApiResponse(responseCode =  "200", description = "All Boards deleted successfully"),
            @ApiResponse(responseCode =  "404", description = "No Board found to delete")
    })
    @DELETE
    public void deleteAll(){
        stateRepository.deleteAll();
    }
}
