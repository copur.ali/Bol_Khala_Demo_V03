package com.bol.challenge.cali;

import com.bol.challenge.cali.entity.BoardState;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.config.LogConfig.logConfig;

@QuarkusTest
@QuarkusTestResource(MongoDbResource.class)
class StateConverterTest {

    @BeforeAll
    static void initAll() {
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config
                .logConfig((logConfig().enableLoggingOfRequestAndResponseIfValidationFails()))
                .objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((type, s) -> new ObjectMapper()
                        .registerModule(new Jdk8Module())
                        .registerModule(new JavaTimeModule())
                        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)));
    }

  @Test
  void testBoardStates() {
    //  performStateTest("/game/boardState");
  }

    private void performStateTest(String path) {
    String currStatee =
        "{ \"gameId\" : \"khala_1\", \"stateId\" : \"1993-05-19\", \"activity\" : \"ACTIVE\",\"createDate\":\"2021-12-31T23:59:59.999\"}";
         //   + " \"player1\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\","
          //  + " \"player2\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\"}";

    String moveState =
            "{ \"gameId\" : \"khala_2\", \"stateId\" : \"1993-05-20\", \"activity\" : \"SUSPENDED\",\"createDate\":\"2021-12-31T23:59:59.999\"}";
               //     + " \"player1\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\","
                //    + " \"player2\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\"}";

    String nextState =
            "{ \"gameId\" : \"khala_3\", \"stateId\" : \"1993-05-21\", \"activity\" : \"COMPLETED\",\"createDate\":\"2021-12-31T23:59:59.999\"}";
                 //   + " \"player1\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\","
                  //  + " \"player2\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\"}";

    RestAssured.given()
                .contentType(ContentType.JSON)
                .body(currStatee)
                .when()
                .post(path)
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(moveState)
                .when()
                .post(path)
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(nextState)
                .when()
                .post(path)
                .then()
                .statusCode(201);

    BoardState[] states = RestAssured.given()
            .when()
            .contentType(ContentType.JSON)
            .get(path)
            .then()
            .statusCode(200)
            .extract()
            .body().as(BoardState[].class);
        Assertions.assertThat(states.length).isEqualTo(3);

        System.out.println("ObjectId: " + states[0].id.toString());
        BoardState state = RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .get(path + "/{id}", states[0].id.toString())
                .then()
                .statusCode(200)
                .extract()
                .body().as(BoardState.class);

        Assertions.assertThat(state.id).isEqualTo(states[0].id);
        Assertions.assertThat(state.getGameId()).isEqualTo(states[0].getGameId());

        state = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .when()
                .get(path + "/search/{gameId}", "khala_1")
                .then()
                .statusCode(200)
                .extract()
                .body().as(BoardState.class);

        Assertions.assertThat(state.getGameId()).isEqualTo("khala_1");


        RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .delete(path + "/{id}", state.id.toString())
                .then()
                .statusCode(204);

        states = RestAssured.given()
                .when()
                .contentType(ContentType.JSON)
                .get(path)
                .then()
                .statusCode(200)
                .extract()
                .body().as(BoardState[].class);

        Assertions.assertThat(states.length).isEqualTo(2);

        RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .delete(path)
                .then()
                .statusCode(204);
}

    @Test
    void createWithGameId() {
    }

    @Test
    void create() {
    }

    @Test
    void findById() {
    }

    @Test
    void move() {
    }

    @Test
    void settlePieces() {
    }

    @Test
    void updateCells() {
    }

    @Test
    void isValidMove() {
    }

}
