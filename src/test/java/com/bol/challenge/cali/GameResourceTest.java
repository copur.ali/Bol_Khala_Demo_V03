package com.bol.challenge.cali;

import com.bol.challenge.cali.entity.Game;
import com.bol.challenge.cali.enums.GameActivity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.config.LogConfig.logConfig;

@QuarkusTest
@QuarkusTestResource(MongoDbResource.class)
class GameResourceTest {

    @BeforeAll
    static void initAll() {
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config
                .logConfig((logConfig().enableLoggingOfRequestAndResponseIfValidationFails()))
                .objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory((type, s) -> new ObjectMapper()
                        .registerModule(new Jdk8Module())
                        .registerModule(new JavaTimeModule())
                        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)));
    }

  @Test
  void testGameEntity() {
    // performGameTest("/bol/games");
  }

    private void performGameTest(String path) {
    String gameActive =
        "{ \"gameId\" : \"khala_1\", \"stateId\" : \"1993-05-19\", \"activity\" : \"ACTIVE\",\"createDate\":\"2021-12-31T23:59:59.999\"}";
         //   + " \"player1\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\","
          //  + " \"player2\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\"}";

    String gameSuspend =
            "{ \"gameId\" : \"khala_2\", \"stateId\" : \"1993-05-20\", \"activity\" : \"SUSPENDED\",\"createDate\":\"2021-12-31T23:59:59.999\"}";
               //     + " \"player1\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\","
                //    + " \"player2\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\"}";

    String gameCompleted =
            "{ \"gameId\" : \"khala_3\", \"stateId\" : \"1993-05-21\", \"activity\" : \"COMPLETED\",\"createDate\":\"2021-12-31T23:59:59.999\"}";
                 //   + " \"player1\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\","
                  //  + " \"player2\" : \"{ \"name\" : \"moncef\", \"birthDate\" : \"1993-05-19\", \"status\" : \"LIVING\"}\"}";

    RestAssured.given()
                .contentType(ContentType.JSON)
                .body(gameActive)
                .when()
                .post(path)
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(gameSuspend)
                .when()
                .post(path)
                .then()
                .statusCode(201);
        RestAssured.given()
                .contentType(ContentType.JSON)
                .body(gameCompleted)
                .when()
                .post(path)
                .then()
                .statusCode(201);

    Game[] games = RestAssured.given()
            .when()
            .contentType(ContentType.JSON)
            .get(path)
            .then()
            .statusCode(200)
            .extract()
            .body().as(Game[].class);
        Assertions.assertThat(games.length).isEqualTo(3);

        System.out.println("ObjectId: " + games[0].id.toString());
        Game game = RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .get(path + "/{id}", games[0].id.toString())
                .then()
                .statusCode(200)
                .extract()
                .body().as(Game.class);

        Assertions.assertThat(game.id).isEqualTo(games[0].id);
        Assertions.assertThat(game.gameId).isEqualTo(games[0].gameId);
        Assertions.assertThat(game.stateId).isEqualTo(games[0].stateId);
        Assertions.assertThat(game.activity).isEqualTo(games[0].activity);
        Assertions.assertThat(game.createDate).isEqualTo("2021-12-31T23:59:59.999");

        game = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .when()
                .get(path + "/search/{gameId}", "khala_1")
                .then()
                .statusCode(200)
                .extract()
                .body().as(Game.class);

        Assertions.assertThat(game.gameId).isEqualTo("khala_1");
        Assertions.assertThat(game.stateId).isEqualTo("1993-05-19");
        Assertions.assertThat(game.activity).isEqualTo(GameActivity.ACTIVE);
        Assertions.assertThat(game.createDate).isEqualTo("2021-12-31T23:59:59.999");


        RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .delete(path + "/{id}", game.id.toString())
                .then()
                .statusCode(204);

        games = RestAssured.given()
                .when()
                .contentType(ContentType.JSON)
                .get(path)
                .then()
                .statusCode(200)
                .extract()
                .body().as(Game[].class);

        Assertions.assertThat(games.length).isEqualTo(2);

        RestAssured
                .given()
                .when()
                .contentType(ContentType.JSON)
                .delete(path)
                .then()
                .statusCode(204);
    }


}
